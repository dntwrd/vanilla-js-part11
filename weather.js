const COORDS_LS = 'coords';
const API_KEY = 'cae425c3454169d9f321cde5356e52a9';
const weatherContainer = document.querySelector('.js-weather');

function getWeather(lat, lng){
    fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&appid=${API_KEY}&units=metric`)
        .then(
            function (response){
                return response.json()
            }
        )
        .then(function (json){
            const temp = json.main.temp;
            const loc = json.name;
            weatherContainer.innerText = `${temp} @ ${loc}`;
        })
}

function saveCoords(positionObj){
    localStorage.setItem(COORDS_LS, JSON.stringify(positionObj));
}

function geoSuccessHandler(position){
    //console.log(position);
    const latitude = position.coords.latitude;
    const longitude = position.coords.longitude;
    const positionObj = {
        latitude,
        longitude
    }
    saveCoords(positionObj);
    getWeather(latitude,longitude);
}

function geoErrorHandler(){
    console.log('errorrrr');
}

function askForCoords(){
    navigator.geolocation.getCurrentPosition(geoSuccessHandler, geoErrorHandler);

}

function getCoords(){
    const coords = localStorage.getItem(COORDS_LS);
    if (coords === null){
        askForCoords();
    } else {
        const loadedCoards = JSON.parse(coords);
        console.log(loadedCoards);
        getWeather(loadedCoards.latitude, loadedCoards.longitude);

    }

}

function init (){
    getCoords();
}

init();